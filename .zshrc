# Zsh configuration

# Set colors.
autoload -U colors
colors

# Set prompt.
PS1="%F{6}%~%f %(?..%B%F{1}%?%f%b )%F{4}∮%f "

# History file configuration.
setopt hist_ignore_dups
SAVEHIST=100000
HISTSIZE=100000
HISTFILE="$XDG_CACHE_HOME/zsh/history"

# Enable tab completion.
autoload -U compinit
zstyle ":completion:*" menu select
zmodload zsh/complist
compinit

# Do not ask before executing `rm *` or `rm path/*`.
setopt rm_star_silent

# vi-like bindings.
bindkey -v "^?" backward-delete-char

# GPG.
export GPG_TTY=$(tty)

# Load aliases.
if [ -f "$XDG_CONFIG_HOME/aliasrc" ]; then
	source "$XDG_CONFIG_HOME/aliasrc"
fi

# Load Zsh scripts.

AUTOSUGGESTIONS="$XDG_DATA_HOME/zsh/autosuggestions/zsh-autosuggestions.zsh"
SYNTAX_HIGHLIGHT="$XDG_DATA_HOME/zsh/syntax-highlighting/zsh-syntax-highlighting.zsh"

if [ -f "$AUTOSUGGESTIONS" ]; then
	source "$AUTOSUGGESTIONS"
fi

# This command must be at the end of the configuration file.
if [ -f "$SYNTAX_HIGHLIGHT" ]; then
	source "$SYNTAX_HIGHLIGHT"
fi
