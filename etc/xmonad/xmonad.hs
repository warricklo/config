-- XMonad configuration

import Control.Monad
import Data.Monoid
import System.Exit

import Graphics.X11.ExtraTypes.XF86

import XMonad
import XMonad.Actions.CycleWS ( nextScreen, prevScreen, shiftNextScreen, shiftPrevScreen )
import XMonad.Actions.Navigation2D
import XMonad.Actions.Promote
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.FadeWindows ( isFloating )
import XMonad.Hooks.InsertPosition
import XMonad.Hooks.ManageDocks ( ToggleStruts(..), docks, avoidStruts )
import XMonad.Hooks.ManageHelpers ( isFullscreen, doFullFloat )
import XMonad.Hooks.StatusBar
import XMonad.Hooks.StatusBar.PP
import XMonad.Layout.Grid
import XMonad.Layout.LayoutCombinators ( JumpToLayout(..) )
import XMonad.Layout.LayoutModifier
import XMonad.Layout.NoBorders ( noBorders )
import XMonad.Layout.Renamed
import XMonad.Layout.Spacing
import XMonad.Layout.ToggleLayouts ( ToggleLayout(..), toggleLayouts )
import XMonad.Util.Run ( safeSpawn )
import XMonad.Util.SpawnOnce

import qualified Codec.Binary.UTF8.String as UTF8
import qualified Data.Map as M
import qualified DBus as D
import qualified DBus.Client as D

import qualified XMonad.StackSet as W

-- Basic configuration

modMask :: KeyMask
modMask = mod4Mask

workspaces :: [String]
workspaces = map show [1 .. 10]

focusFollowsMouse, clickJustFocuses :: Bool
focusFollowsMouse = False
clickJustFocuses = False

borderWidth :: Dimension
borderWidth = 1
normalBorderColor, focusedBorderColor :: String
normalBorderColor = "#101010"
focusedBorderColor = "#fc9867"

-- Default geometry of a floating window.
windowSize = W.RationalRect 0.2 0.2 0.6 0.6

terminal :: String
terminal = "alacritty"

-- 2D navigation

-- Window navigation in the Full layout doesn't work when noBorders is used.
navigation2DConfig :: Navigation2DConfig
navigation2DConfig = def
    { defaultTiledNavigation = centerNavigation
    , floatNavigation = centerNavigation
    , screenNavigation = lineNavigation
    , layoutNavigation = [("Full", centerNavigation)]
    , unmappedWindowRect = [("Full", singleWindowRect)]
    }

-- Key bindings

keys :: XConfig Layout -> M.Map (KeyMask, KeySym) (X ())
keys conf@(XConfig {XMonad.modMask = modm}) = M.fromList $
    [ ((modm .|. controlMask, xK_Escape), io exitSuccess)
    , ((modm .|. mod1Mask, xK_Escape), spawn "xmonad --recompile && xmonad --restart")

    , ((modm, xK_BackSpace), kill)
    , ((modm .|. shiftMask, xK_c), kill)

    -- Switching layouts.
    , ((modm, xK_Tab), sendMessage NextLayout)
    , ((modm, xK_f), toggleFull)
    , ((modm, xK_t), sendMessage $ JumpToLayout "Tall")
    , ((modm, xK_g), sendMessage $ JumpToLayout "Grid")
    , ((modm, xK_m), sendMessage $ JumpToLayout "Monocle")
    , ((modm .|. mod1Mask, xK_space), withFocused toggleFloat)

    -- Focus and swap between monitors.
    , ((modm, xK_comma), prevScreen)
    , ((modm, xK_period), nextScreen)
    , ((modm .|. shiftMask, xK_comma), shiftPrevScreen)
    , ((modm .|. shiftMask, xK_period), shiftNextScreen)

    -- Change size of the master pane.
    , ((modm, xK_bracketleft), sendMessage Shrink)
    , ((modm, xK_bracketright), sendMessage Expand)

    -- Change the number of windows in the master pane.
    , ((modm .|. shiftMask, xK_bracketleft), sendMessage $ IncMasterN 1)
    , ((modm .|. shiftMask, xK_bracketright), sendMessage $ IncMasterN (-1))

    -- Change the size of the window and screen spacing.
    , ((modm, xK_minus), decWindowSpacing 4 >> decScreenSpacing 4)
    , ((modm, xK_equal), incWindowSpacing 4 >> incScreenSpacing 4)

    -- Promote window to the master pane. If the window is in the master pane,
    -- swap it with the next window in the stack.
    , ((modm, xK_p), promote)

    -- Switch the layer of focus for directional navigation.
    , ((modm .|. controlMask, xK_space), switchLayer)

    -- Focus and swap windows using directional navigation.
    , ((modm, xK_Up), windowGo U False)
    , ((modm, xK_Down), windowGo D False)
    , ((modm, xK_Left), windowGo L False)
    , ((modm, xK_Right), windowGo R False)
    , ((modm, xK_k), windowGo U False)
    , ((modm, xK_j), windowGo D False)
    , ((modm, xK_h), windowGo L False)
    , ((modm, xK_l), windowGo R False)
    , ((modm .|. shiftMask, xK_Up), windowSwap U False)
    , ((modm .|. shiftMask, xK_Down), windowSwap D False)
    , ((modm .|. shiftMask, xK_Left), windowSwap L False)
    , ((modm .|. shiftMask, xK_Right), windowSwap R False)
    , ((modm .|. shiftMask, xK_k), windowSwap U False)
    , ((modm .|. shiftMask, xK_j), windowSwap D False)
    , ((modm .|. shiftMask, xK_h), windowSwap L False)
    , ((modm .|. shiftMask, xK_l), windowSwap R False)

    -- Menus and widgets.
    , ((modm, xK_Escape), spawn "$HOME/bin/sysmenu")
    , ((modm, xK_space), safeSpawn "rofi" ["-show", "drun"])
    , ((modm .|. shiftMask, xK_space), safeSpawn "rofi" ["-show", "run"])

    -- Run programs.
    , ((modm, xK_Return), safeSpawn Main.terminal [])
    , ((modm .|. mod1Mask, xK_b), safeSpawn "brave" [])
    , ((modm .|. mod1Mask, xK_e), safeSpawn "emacs" [])
    , ((modm .|. mod1Mask, xK_r), runInTerm "nvim")
    , ((modm .|. mod1Mask, xK_d), safeSpawn "nemo" [])
    , ((modm .|. mod1Mask, xK_f), runInTerm "nnn")
    , ((modm .|. mod1Mask, xK_n), runInTerm "neomutt")
    , ((modm .|. mod1Mask, xK_m), safeSpawn "evolution" [])

    -- Screenshot entire desktop and save image file.
    , ((0, xK_Print),
        spawn "$HOME/bin/screenshot full")
    -- Select region, screenshot, and save image file.
    , ((modm, xK_Print),
        spawn "$HOME/bin/screenshot region")
    -- Select region, screenshot, save image file, and copy to clipboard.
    , ((shiftMask, xK_Print),
        spawn "$HOME/bin/screenshot region clipboard")

    -- Multimedia keys.
    , ((0, xF86XK_AudioMute),
        spawn "$HOME/bin/vol toggle")
    , ((0, xF86XK_AudioLowerVolume),
        spawn "$HOME/bin/vol lower 1")
    , ((0, xF86XK_AudioRaiseVolume),
        spawn "$HOME/bin/vol raise 1")
    , ((shiftMask, xF86XK_AudioLowerVolume),
        spawn "$HOME/bin/vol lower 10")
    , ((shiftMask, xF86XK_AudioRaiseVolume),
        spawn "$HOME/bin/vol raise 10")
    , ((0, xF86XK_AudioPlay),
        spawn "mpc toggle")
    , ((0, xF86XK_AudioStop),
        spawn "mpc stop")
    , ((0, xF86XK_AudioPrev),
        spawn "mpc seek -10")
    , ((0, xF86XK_AudioNext),
        spawn "mpc seek +10")
    , ((shiftMask, xF86XK_AudioPrev),
        spawn "mpc prev")
    , ((shiftMask, xF86XK_AudioNext),
        spawn "mpc next")
    ]
    ++
    [((m .|. modm, k), windows $ f i)
        | (i, k) <- zip (XMonad.workspaces conf) ([xK_1 .. xK_9] ++ [xK_0])
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]
    ]
  where
    -- Run the given command in the configured terminal emulator.
    -- Reimplementation of XMonad.Util.Run.safeRunInTerm because the function
    -- appears to not work when passing arguments into the terminal emulator.
    runInTerm :: String -> X ()
    runInTerm command = safeSpawn Main.terminal ["-e", command]

    -- Toggle struts and fullscreen layout.
    toggleFull :: X ()
    toggleFull = sendMessage ToggleStruts >> sendMessage (Toggle "Full")

    -- Toggle floating for the given window.
    toggleFloat :: Window -> X ()
    toggleFloat w = do
        floating <- runQuery isFloating w
        windows (\s ->
            if floating
                then W.sink w s
                else W.float w windowSize s)

mouseBindings :: XConfig Layout -> M.Map (KeyMask, Button) (Window -> X ())
mouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList
    [ ((modm, button1), \w -> focus w >> centerAndFloat w >> mouseMoveWindow w >> windows W.shiftMaster)
    , ((modm, button2), \w -> focus w >> windows W.shiftMaster)
    , ((modm, button3), \w -> focus w >> mouseResizeWindow w >> windows W.shiftMaster)
    ]
  where
    -- Float and center a window only if it is already tiling.
    centerAndFloat :: Window -> X ()
    centerAndFloat w = do
        floating <- runQuery isFloating w
        unless floating (windows (W.float w windowSize))

-- Layouts

layoutHook =
    avoidStruts
    $ toggleLayouts full
    $ tall ||| grid ||| monocle
  where
    -- Add spacing along the edges of the screen and around windows.
    layoutSpacing :: Integer -> l a -> ModifiedLayout Spacing l a
    layoutSpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True

    tall = renamed [Replace "Tall"]
        $ layoutSpacing 4
        $ Tall 1 (1/100) (toRational (2/(1+sqrt(5))))  -- Use the inverse golden ratio.
    grid = renamed [Replace "Grid"]
        $ layoutSpacing 4
        $ GridRatio (3/2)
    monocle = renamed [Replace "Monocle"]
        $ Full
    full = renamed [Replace "Full"]
        $ noBorders
        $ Full

-- Startup

startupHook :: X ()
startupHook = do
    spawnOnce "picom --experimental-backends --config $XDG_CONFIG_HOME/picom/picom.conf"
    spawnOnce "dunst"
    spawnOnce "eww daemon"
    spawn "/bin/sh $HOME/bin/polybar"  -- The script needs to use /bin/sh.
    spawn "$HOME/bin/bg"

-- Window rules

manageHook :: ManageHook
manageHook = composeAll
    [ insertPosition Below Newer
    , isFullscreen --> doFullFloat
    , appName =? "desktop_window" --> doIgnore
    , stringProperty "_NET_WM_WINDOW_TYPE" =? "_NET_WM_WINDOW_TYPE_DIALOG" --> doFloat
    , className =? "Xmessage" --> doFloat
    , title =? "Picture-in-picture" --> doFloat  -- Picture-in-picture windows.
    , className =? "Gimp" --> doFloat  -- Gimp windows.
    ]

-- Log hook

dbusOutput :: D.Client -> String -> IO ()
dbusOutput dbus str =
    let
        objectPath = D.objectPath_ "/org/xmonad/Log"
        interfaceName = D.interfaceName_ "org.xmonad.Log"
        memberName = D.memberName_ "Update"
        signal = D.signal objectPath interfaceName memberName
        body = [D.toVariant $ UTF8.decodeString str]
    in
        D.emit dbus $ signal { D.signalBody = body }

-- Dynamic log with pretty printing format, outputted to DBus.
dbusLogHook :: D.Client -> PP
dbusLogHook dbus = def
    { ppOrder = \(_:l:_:_) -> [l]  -- Only output the layout.
    , ppOutput = dbusOutput dbus
    }

-- Main

main :: IO ()
main = do
    dbus <- D.connectSession
    -- Request access to the DBus name.
    D.requestName dbus (D.busName_ "org.xmonad.Log")
        [D.nameAllowReplacement, D.nameReplaceExisting, D.nameDoNotQueue]

    xmonad $ ewmh $ ewmhFullscreen $ docks $ withNavigation2DConfig navigation2DConfig $ def
        { XMonad.modMask = Main.modMask
        , XMonad.workspaces = Main.workspaces
        , XMonad.focusFollowsMouse = Main.focusFollowsMouse
        , XMonad.clickJustFocuses = Main.clickJustFocuses
        , XMonad.borderWidth = Main.borderWidth
        , XMonad.normalBorderColor = Main.normalBorderColor
        , XMonad.focusedBorderColor = Main.focusedBorderColor

        , XMonad.terminal = Main.terminal

        , XMonad.keys = Main.keys
        , XMonad.mouseBindings = Main.mouseBindings

        , XMonad.layoutHook = Main.layoutHook
        , XMonad.startupHook = Main.startupHook
        , XMonad.manageHook = Main.manageHook
        , logHook = dynamicLogWithPP (dbusLogHook dbus)
        }
